﻿using UnityEngine;
using System.Collections;
using Vuforia;
using AssemblyCSharp;

public class Imagem : MonoBehaviour, ITrackableEventHandler {

	#region Variaveis que cuidam do tempo
	private int contador;
	private float tempo;
	public int QuantidadeDeFrame;
	#endregion

	private TrackableBehaviour mTrackableBehaviour;
	public Material verde;
	private Vector3 posAnterior;
	public GameObject bolaBranca;
	private bool encontrado = false;

	private bool apresentou;
	private MomentosDoTaco momento1 = null;
	private MomentosDoTaco momento2 = null;

	void Start()
	{
		mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}
		contador = 0;
		posAnterior = this.transform.position;

		//direcao2 = new Vector3(0,0,0);
		//direcao1 = new Vector3(0,0,0);
	}

	private void CriarPonto(Vector3 position){
		var cube = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		cube.transform.position = position; //new Vector3 (3f, 100f, 0f);
		cube.transform.localScale = new Vector3 (0.1f, 0.1f, 0.1f);
		Renderer r = cube.GetComponent<Renderer> ();
		if (r != null) {
			r.material = verde;
		}
	}

	// Update is called once per frame
	void Update () {

		#region Primeiro Teste Para pegar a direção do taco e aplicar um força fixa na bola na direção do taco
//		//Debug.Log("ggggggggggggggggggggggggggggggggggggggg" + this.transform.rotation.eulerAngles);
//
//		// busca a direção do taco
//		Vector3 direcao = getDirecao(this.transform.rotation.eulerAngles);
//
//		// Tamanho da linha vermelha na ponta do taco
//		float dif = 8f;
//		// Cria a linha vermelha na ponta do taco
//		Debug.DrawLine (this.transform.position, this.transform.position + direcao * dif, Color.red);
//
//		// Aplica uma força fixa de 5 na direção do taco
//		bolaBranca.GetComponent<Rigidbody> ().AddForce (direcao * 5f);
		#endregion


		contador++;
		tempo += Time.deltaTime;
		if (contador >= QuantidadeDeFrame) { // Foi usado essa contado, que será ajustado, para deixar um tempo entre os frames
			if (momento1 == null)
				momento1 = new MomentosDoTaco (this.transform.position, Direcao.getDirecao (this.transform.rotation.eulerAngles));
			else {
				momento2 = new MomentosDoTaco (this.transform.position, Direcao.getDirecao (this.transform.rotation.eulerAngles));

				//float diff = Vector3.Distance (posAnterior, posAtual);
				// Direção entre os dois pontos
				Vector3 direcaoDoMovimento = Direcao.getDirecao(momento1.posicao, momento2.posicao);

				Debug.ClearDeveloperConsole ();
				Debug.Log("Dir. Do Taco:      " + momento2.direcao);
				Debug.Log("Dir. Do Movimento: " + direcaoDoMovimento);

				if (Direcao.alterouDirecao (momento1.direcao, momento2.direcao) 
					|| Direcao.alterouDirecao(momento2.direcao, direcaoDoMovimento)) {

					momento1 = momento2;
					momento2 = null;
					foreach (Object ob in GameObject.FindGameObjectsWithTag ("Ponto")) {
						GameObject.DestroyImmediate (ob);
					}
				}
			}
//			if (direcao1 == new Vector3(0,0,0))
//				direcao1 = Direcao.getDirecao(this.transform.rotation.eulerAngles);
//			else 
//				direcao2 = Direcao.getDirecao(this.transform.rotation.eulerAngles);
			
			Vector3 posAtual = this.transform.position; // Posição atual do taco
			float dif = Vector3.Distance (posAnterior, posAtual); // Posição anterior é a ultima posição do taco encontrado pelo vuforia
			float velocidade = dif * tempo; // Tempo passado para encontrar um ponto e o outro
			float force = velocidade * 10; // Esse valor 10 será ajustado para ficar mais realista

			// Linha abaixo comentada pois está juntando os códigos já testados
			//Vector3 direcao = Vector3.forward; // A direção por enquanto está fixa para um lado, porém devera ser encontrado essa direção
			// Pegando a direção do taco
			Vector3 direcao = Direcao.getDirecao(this.transform.rotation.eulerAngles);

			bolaBranca.GetComponent<Rigidbody> ().AddForce (direcao * force); // Aplicar o força na bola
			posAnterior = posAtual;
			contador = 0;
			tempo = 0;


		}

//		if (Direcao.alterouDirecao (momento1.direcao, momento2.direcao)) {
//			Debug.Log ("Alterou direção " + momento1.direcao + " - " + momento2.direcao);
//		} else {
//			Debug.Log ("NÃO Alterou direção " + momento1.direcao + " - " + momento2.direcao);
//		}

		#region Teste da classe RaycastHit
//		// Foi criado dois pontos fixos
//		posAnterior = new Vector3 (3f, 107.5f, 4);
//		Vector3 posAtual = new Vector3 (8f, 107.5f, 8);
//		// Apresenta eles na tela
//		if (!apresentou)
//		{
//			apresentou = true;
//			CriarPonto (posAnterior);
//			CriarPonto (posAtual);
//		}
//
//		// Distancia entre os pontos
//		float diff = Vector3.Distance (posAnterior, posAtual);
//		// Direção entre os dois pontos
//		Vector3 dir = posAtual - posAnterior;
//
//		Debug.Log("ggggggggggggggggggggggggggggggggggggggg" + dir);
//
//		// Monta o raycast
//		RaycastHit[] t =  Physics.RaycastAll (posAnterior, dir, diff);
//		// Monta a linha para visualizar
//		Debug.DrawLine (posAnterior, posAnterior + dir, Color.red); 
//
//		// Verifica os o58
//		foreach (RaycastHit f in t) {
//			Debug.Log ("kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk " + f.transform.name);
//		}
		#endregion


		#region Teste de rastreabilidade
		// Esse código para verificar a rastreabilidade do vuforia para identificar o image target
		//if (encontrado)
		//	CriarPonto (this.transform.position);
		#endregion
	}

	public void OnTrackableStateChanged(
		TrackableBehaviour.Status previousStatus,
		TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
			newStatus == TrackableBehaviour.Status.TRACKED ||
			newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			//Debug.Log ("Encontrou");
			encontrado = true;
		}
		else
		{
			//Debug.Log ("PErdeu");
			encontrado = false;
		}
	}

}
