﻿using System;
using UnityEngine;

namespace AssemblyCSharp
{
	public class MomentosDoTaco
	{
		public Vector3 posicao;
		public Vector3 direcao;

		public MomentosDoTaco (Vector3 posicao, Vector3 direcao)
		{
			this.posicao = posicao;
			this.direcao = direcao;

			CriarPonto (this.posicao);
		}

		private void CriarPonto(Vector3 position){
			var cube = GameObject.CreatePrimitive (PrimitiveType.Sphere);
			cube.tag = "Ponto";
			cube.transform.position = position; //new Vector3 (3f, 100f, 0f);
			cube.transform.localScale = new Vector3 (0.1f, 0.1f, 0.1f);
		}
	}
}

