﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Tempo : MonoBehaviour {

    Image fillImg;
    public bool iniciar;

    private float current;
    public float speed;

    // Use this for initialization
    void Start()
    {
        fillImg = this.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (iniciar)
        {
            if (current < 100)
            {
                current += speed * Time.deltaTime;
            }
            else
            {
                Debug.Log("OK tempo finalizado!");
            }
            fillImg.fillAmount = current / 100;
        }
        else
        {
            fillImg.fillAmount = 0;
            current = 0;
        }
    }
}
