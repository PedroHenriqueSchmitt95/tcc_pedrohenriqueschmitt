﻿using UnityEngine;
using System;
using System.Collections;

public class PlacarController : MonoBehaviour {

	/*
	 * Classe cuida do placar superior no canto da tela
	*/

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		var text = GetComponent<UnityEngine.UI.Text>();
		var currentPlayer = PoolGameController.GameInstance.CurrentPlayer;
		var otherPlayer = PoolGameController.GameInstance.OtherPlayer;
		text.text = String.Format("* {0} - {1} ({2}) / {3} - {4} ({5})", 
			currentPlayer.Name, 
			currentPlayer.Pontos,
			(currentPlayer.TipoDeBola == TipoBola.Lisa?"Lisa":(currentPlayer.TipoDeBola == TipoBola.Listrada?"List":"")),
			otherPlayer.Name, 
			otherPlayer.Pontos,
			(otherPlayer.TipoDeBola == TipoBola.Lisa?"Lisa":(otherPlayer.TipoDeBola == TipoBola.Listrada?"List":"")));
	}
}
