﻿using UnityEngine;
using System.Collections;

namespace GameStates {
	public class WaitingForNextTurnState : AbstractGameObjectState {
		private PoolGameController gameController;
		private GameObject taco;
		private GameObject bolaBranca;
		private GameObject bolas;

		private Vector3 cueOffset;
		private Quaternion cueRotation;

		public WaitingForNextTurnState(MonoBehaviour parent) : base(parent) {
			gameController = (PoolGameController)parent;

			taco = gameController.taco;
			bolaBranca = gameController.bolaBranca;
			bolas = gameController.bolas;
			 
			cueOffset = bolaBranca.transform.position - taco.transform.position;
			cueRotation = taco.transform.rotation;
		}

		public override void FixedUpdate() {
			//Debug.Log(redBalls.GetComponentsInChildren<Transform>().Length);
				#region Verirfica se todas as bolas estao paradas para liberar o movimento do taco
				var cueBallBody = bolaBranca.GetComponent<Rigidbody>();
				if (!(cueBallBody.IsSleeping() || cueBallBody.velocity == Vector3.zero))
					return;
				
				foreach (var rigidbody in bolas.GetComponentsInChildren<Rigidbody>()) {
					if (!(rigidbody.IsSleeping() || rigidbody.velocity == Vector3.zero))
						return;
				}
				#endregion

				//Verifica se as regras foram atingindas
				gameController.VerificaRegrasDaTacada();

				// Busca proximo jogador
				gameController.NextPlayer();
				// Libera o movimento do taco para o jogador
				gameController.estadoAtual = new DirecionandoTacoState(gameController);
			
		}

		public override void LateUpdate() {
			
			taco.transform.position = bolaBranca.transform.position - cueOffset;
			taco.transform.rotation = cueRotation;
		}
	}
}